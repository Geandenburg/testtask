$(document).ready(function(){
			//реализация проверки полей.
				var $correctName=0,
					$correctEmail=0,
					$correctPass=0,
					$correctCheckPass=0;
					$checkLogEmail=0,
					$checkLogPass=0;
					$checkLogCapcha=0;
				$('#inputRegName').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
						var pattern = /[a-zA-Zа-яёА-ЯЁ0-9]$/;
						if(pattern.test($(this).val())){
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Верно';
							$correctName=1;
							
						} else {
							$(this).css({'border' : '1px solid #ff0000'});
							//$('#valid1').text('Не верно');
							$textError='В имени могут быть только буквы и цифры';
							$correctName=0;
						}
					} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$correctName=0;
					}
					
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				});
				$('#inputRegEmail').blur(function(){
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
						var pattern = /^\w+([\.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}$/,
							Email=$(this).val().toLowerCase();
						if(pattern.test(Email)){
							$.ajax({
								async: false,
								url: "vendors/ajax/email_check.php",
								type: "POST",
								data: {ThisEmail:Email},
								success: function(data) {
									if (data == 0){
										$('#inputRegEmail').css({'border' : '1px solid #569b44'});
										//$('#valid1').text('Верно');
										$textError='Верно';
										$correctEmail=1;
									}
									if (data == 1){
										$('#inputRegEmail').css({'border' : '1px solid #ff0000'});
										//$('#valid1').text('Не верно');
										$textError='Такой e-mail уже есть';
										$correctEmail=0;
									}
								}
							});							
						} else {
							$(this).css({'border' : '1px solid #ff0000'});
							//$('#valid1').text('Не верно');
							$textError='Введите корректный e-mail';
							$correctEmail=1;
						}
					} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$correctEmail=0;
					}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				});
				$('#inputRegPass').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
						var pattern = /^(?=.*[0-9])(?=.*[\!\@\#\$\%\^\&\*])[a-zA-Z0-9\!\@\#\$\%\^\&\*]{6,}$/;
						if(pattern.test($(this).val())){
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Верно';
							$correctPass=1;
							
						} else {
							$(this).css({'border' : '1px solid #ff0000'});
							//$('#valid1').text('Не верно');
							$textError='Введите корректный пароль';
							$correctPass=0;
						}
					} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$correctPass=0;
					}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				});
				$('#checkRegPass').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
						if($(this).val() === $('#inputRegPass').val()){
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Верно';
							$correctCheckPass=1;
							
						} else {
							$(this).css({'border' : '1px solid #ff0000'});
							//$('#valid1').text('Не верно');
							$textError='Не совпадает';
							$correctCheckPass=0;
						}
					} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$correctCheckPass=0;
					}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				});
				//это будет включаться после проверки переменных состояния полей.
				var reg = $(".reg");
				reg.click(function(){
					if ($correctName==1 && $correctEmail==1 && $correctPass==1 && $correctCheckPass==1){
						var inputName = $("#inputRegName").val(),
							inputEmail = $("#inputRegEmail").val().toLowerCase(),
							inputPass = $("#inputRegPass").val(),
							checkPass = $("#checkRegPass").val();
						$.ajax({
							url: "vendors/ajax/action.php",
							type: "POST",
							data: {RegName:inputName, RegEmail:inputEmail, RegPass:inputPass, CheckPass:checkPass},
							success: function(data) {
								if (data == 0){
									alert("Вы успешно зарегистрировались!");
								}
								if (data == 1){
									alert("Произошла ошибка пр регистрации. Пожалуйста, проверьте данные регистрации для устраниения ошибок.");
								}
							}
						})
					} else{
						alert("Пожалуйста, заполните форму регистрации для продолжения.");
					}

				}) 
				$('#inputLogEmail').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Поле заполнено';
							$checkLogEmail=1;
						} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$checkLogEmail=0;
						}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				})
				$('#inputLogPass').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Поле заполнено';
							$checkLogPass=1;
						} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$checkLogPass=0;
						}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				})
				$('#inputLogCapcha').blur(function() {
					var $textError='text';
					$(this).tooltip('destroy');
					if($(this).val() != '') {
							$(this).css({'border' : '1px solid #569b44'});
							//$('#valid1').text('Верно');
							$textError='Поле заполнено';
							$checkLogCapcha=1;
						} else {
						$(this).css({'border' : '1px solid #ff0000'});
						//$('#valid1').text('Поле не должно быть пустым');
						$textError='Поле не должно быть пустым';
						$checkLogCapcha=0;
						}
					$(this).tooltip({
						trigger: 'manual',
						placement: 'right',
						title: $textError
					}).tooltip('show');
				})
				var autorized = $(".autorized");
				autorized.click(function(){
					if ($checkLogPass==1 && $checkLogEmail==1 && $checkLogCapcha==1){
							var pattern_email = /^\w+([\.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}$/;
							if(pattern_email.test($('#inputLogEmail').val().toLowerCase())){
								var pattern_pass = /^(?=.*[0-9])(?=.*[\!\@\#\$\%\^\&\*])[a-zA-Z0-9\!\@\#\$\%\^\&\*]{6,}$/;
								if(pattern_pass.test($('#inputLogPass').val())){
									var LogCheckEmail = $("#inputLogEmail").val().toLowerCase(),
										LogCheckCapcha = $("#inputLogCapcha").val(),
										LogCheckPass = $("#inputLogPass").val();
										$.ajax({
											url: "vendors/ajax/check_loginfo_capcha.php",
											type: "POST",
											data: {ThisEmail:LogCheckEmail, ThisPass:LogCheckPass, ThisCapcha:LogCheckCapcha},
											success: function(data) {
												if (data == 0){
													alert("Такого пользователя не существует!");
													window.location.href='refresh.php';
												}else{
													if (data==1){
														window.location.href='report.php';
													}
													else{
														alert("Капча введена неверно!");
														window.location.href='refresh.php';
													}
												}
											}
										})	
								}else{
									alert("Такого пользователя не существует");
									window.location.href='refresh.php';
								}
							}else{
								alert("Такого пользователя не существует");
								window.location.href='refresh.php';
							}
					}else{
						alert("Вы не заполнили все поля формы для входа.");
					}
						
						
						
						
				})
			})