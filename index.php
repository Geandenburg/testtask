<?php
	session_start();
	if (!isset($_SESSION['try']))
	{
		$_SESSION['try']=0;
	}
	
	if ($_SESSION['try']>2)
	{
		// набор символов 
		$alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
		$secret = ""; 
		// формируем строку из 5 символов, которая будет отображаться на картинке 
		for($i=0;$i<5;$i++) 
			$secret.= $alpha[rand(0,strlen($alpha)-1)]; 
		// генерируем новый SESSIONID для того, чтобы при 
		// перезагрузке страницы старая сессия затиралась 
		session_id(md5(microtime()*rand()));  
		// сохраняем сгенерированную строку в переменной сессии 
		$_SESSION['secret'] = $secret; 
		require('incorrect_try.html');
	}
	else
	{
		require('correct_try.html');
	}
?>
